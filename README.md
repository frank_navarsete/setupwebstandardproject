This project will create a simple WebStandard project.

It will create the following 

```
src\index.html
src\app.js
src\style.css
```

Simply add this project to your PATH environment.
Remember to make the file ```setupWebStandardProject.sh``` executable. 
Run this command to make it executable ```chmod +x setupWebStandardProject.sh```.

To create a project just type ```setupWebStandardProject.sh```. This will copy the files inside the current directory.

If you type anything after ```setupWebStandardProject.sh``` it will create a directory in the current directory and copy the files inside that directory.