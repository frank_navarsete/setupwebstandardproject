#!/bin/bash
#Fetch project input if available
PROJECT_NAME=$1;

#Fetch location for template-files
SCRIPT_PATH="`dirname \"$0\"`";
TEMPLATE_PATH="$SCRIPT_PATH/template/.";

CURRENT_DIR=$PWD;

#Copy templatefiles to current or project directory base on input
if [ -z "$PROJECT_NAME" ]
then
    #no input
    cp -R $TEMPLATE_PATH $CURRENT_DIR
else
    mkdir $PROJECT_NAME
    cp -R $TEMPLATE_PATH $CURRENT_DIR/$PROJECT_NAME
fi
